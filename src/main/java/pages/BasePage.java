package pages;


import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.*;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class BasePage {

    protected AndroidDriver driver;


    public BasePage(AndroidDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofMillis(10000)), this);
    }

    public void scrollAndClick(String text) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))").click();
    }

    public boolean isElementDisplayed(AndroidElement androidElement) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(androidElement));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void pressBackButton(int numberOfTimes) {
        for(int i = 0; i<numberOfTimes; i++){
            driver.pressKey(new KeyEvent(AndroidKey.BACK));
        }
    }


    public void goToUrl(String url){
        driver.get(url);
    }

    public void consumeIrrelevantLogs() {
        driver.manage().logs().get("logcat");
    }


    public void changeContext(String context){
        Set<String> contextName = driver.getContextHandles();
        for(String contexts : contextName){
            if(contextName.contains("NATIVE_APP")&& context == "NATIVE"){
                driver.context("NATIVE_APP");
            }
            if(contextName.contains("CHROMIUM")&& context == "WEBVIEW") {
                driver.context("CHROMIUM");
            }
        }
    }


    public WebElement findElementWaitVisibility(By locator){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public List<WebElement> findElementsWaitPresence(By locator){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }


    public List getBrowserLogs(){
        List<String> logs = new ArrayList<String>();
        for(LogEntry entry: driver.manage().logs().get("browser")){
            logs.add(entry.getMessage());
        }
        return logs;
    }

}
