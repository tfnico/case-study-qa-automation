package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class FilterOptionsPage extends BasePage {
    public FilterOptionsPage(AndroidDriver driver) {
        super(driver);
    }

    By blockedImg = By.cssSelector("[data-expectedresult='fail']");

    public int getNumberOfBlockedImages() {
        List<WebElement> elements = findElementsWaitPresence(blockedImg);
        List<String> logs = getBrowserLogs();
        int numberOfBlockedImgs = 0;
        for (WebElement element : elements) {
            String imgUrl = element.getAttribute("src");
            for (String line : logs) {
                if (line.contains("Failed to load") && line.contains(imgUrl)) {
                    numberOfBlockedImgs++;
                }
            }
        }
        return numberOfBlockedImgs;
    }

    private int getNumberOfHiddenElements(By locator) {
        List<WebElement> elements = findElementsWaitPresence(locator);
        int numberOfHiddenImgs = 0;
        for (WebElement element : elements) {
            if(!element.isDisplayed()){
                numberOfHiddenImgs++;
            }
        }
        return numberOfHiddenImgs;
    }

    public int getNumberOfHiddenImages(){
        return getNumberOfHiddenElements(blockedImg);
    }

}
