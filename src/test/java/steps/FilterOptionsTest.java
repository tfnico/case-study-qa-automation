package steps;

import io.cucumber.java.en.Then;
import org.junit.Assert;
import pages.FilterOptionsPage;

public class FilterOptionsTest {
    protected BaseTest baseTest = new BaseTest();
    protected FilterOptionsPage filterOptionsPage = new FilterOptionsPage(baseTest.getDriver());


    @Then("I verify {int} image(s) (were)(was) blocked")
    public void verifyImagesBlocked(int expected){
        int actual = filterOptionsPage.getNumberOfBlockedImages();
        Assert.assertEquals("Test failed. Number of blocked images are not correct.",expected,actual);
    }

    @Then("I verify {int} image(s) (were)(was) hidden")
    public void verifyImagesHidden(int expected){
        int actual = filterOptionsPage.getNumberOfHiddenImages();
        Assert.assertEquals("Test failed. Number of hidden images are not correct.",expected,actual);
    }

}
