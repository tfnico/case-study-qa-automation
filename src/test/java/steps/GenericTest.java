package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import org.junit.Assert;
import pages.GenericPage;


public class GenericTest {
    protected BaseTest baseTest = new BaseTest();
    protected GenericPage genericPage = new GenericPage(baseTest.getDriver());

    @Given("I open ABP Chromium and Google Homepage is displayed")
    public void iAmHomePage() {
        genericPage.iAmHomePage();
    }

    @And("I go to AdBlocking Settings")
    public void goToAdBlockSettings() {
        genericPage.goToAdBlockSettings();
    }

    @Then("I add {string} filter list URL")
    public void addFilterList(String url) {
        genericPage.goToAdBlockSettings();
        genericPage.enableMoreOptions();
        genericPage.scrollAndClick("More blocking options");
        genericPage.clickFilter();
        genericPage.sendTextToElement(url);
        genericPage.clickPlusBtn();
    }

    @And("I am at {string} test page")
    public void goToTestPages(String filter){
        genericPage.goToContext("WEBVIEW");
        genericPage.goToUrl("https://testpages.adblockplus.org/en/filters/"+filter);
        String title = genericPage.getFiltersPageTitle().toLowerCase();
        Assert.assertEquals(filter, title);
    }

    @And("I go back to the Main Page")
    public void goBackToMainPage() {
        genericPage.pressBackButton(4);
    }

}
