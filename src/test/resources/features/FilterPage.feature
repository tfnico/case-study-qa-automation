Feature: ABP Chromium

  Scenario: Verify Blocking filters are working properly
    Given I open ABP Chromium and Google Homepage is displayed
    And I add "https://testpages.adblockplus.org/en/abp-testcase-subscription.txt" filter list URL
    Then I go back to the Main Page
    Then I am at 'blocking' test page
    And I verify 5 images were blocked
    And I verify 5 images were hidden
